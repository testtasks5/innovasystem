﻿namespace CycleTimer;
public class CycleTimer
{
    public string Name { get; set; }
    private string path = "log.txt";
    public CycleTimer()
    {
        Name = "CycleTimer";
    }
    public void Timing()
    {
        for(int i = 0; i < 10; i++)
        {
            DateTime dt = DateTime.Now;
            string data = $"{Name} - {dt.Hour}:{dt.Minute} - {dt.Second}";
            Write(data);
            Console.WriteLine($"{Name} - {dt.Hour}:{dt.Minute} - {dt.Second}");
            Thread.Sleep(1000);
        }
    }
    public void Write(string data)
    {
        try
        {
            TextWriter sw = new StreamWriter(path, true);
            sw.WriteLine(data);
            sw.Close();
        }
        catch(Exception e)
        {
            Console.WriteLine("Error" + e.Message);
        }
    }
}
