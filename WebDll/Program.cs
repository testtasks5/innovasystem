using Microsoft.OpenApi.Models;
using WebDll;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Innovasystem API", Description = "Keep track of your tasks", Version = "v1" });
});

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(c =>
  {
     c.SwaggerEndpoint("/swagger/v1/swagger.json", "Inovasystem API V1");
     c.RoutePrefix = string.Empty;
  });
app.MapGet("/", () => "Hello World!");
app.MapPost("/start", (string nameLib) => Controller.Post(nameLib));
app.MapDelete("/delete", () => Controller.Delete());

app.Run();
