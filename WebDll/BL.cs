using System.Reflection;

namespace WebDll;

public class BL
{
    private dynamic oDynamic;
    private Thread workThread;
    private string path = "LogAction.txt";
    
    private void Init(string nameLib)
    {
        string fullpath = Path.GetFullPath(nameLib + ".dll");
        //string nameMethod = "Timing";
        Assembly assembly = Assembly.LoadFile(fullpath);
        Type type = assembly.GetType($"{nameLib}.{nameLib}");
        oDynamic = Activator.CreateInstance(type);
    }
    public void StartTimer(string nameLib)
    {
        Init(nameLib);
        try
        {
            DateTime dt = DateTime.Now;
            string data = $"Start logger at {dt.Hour}:{dt.Minute} - {dt.Second}";
            Write(data);
            workThread = new Thread(ReadDll);
            workThread.Start();
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex.Message);
            DateTime dt = DateTime.Now;
            string data = $"Restart logger at {dt.Hour}:{dt.Minute} - {dt.Second}";
            Write(data);
            workThread = new Thread(ReadDll);
            workThread.Start();
        }
    }

    public void StopTimer()
    {
        DateTime dt = DateTime.Now;
        string data = $"Stop logger at {dt.Hour}:{dt.Minute} - {dt.Second}";
        Write(data);
        oDynamic.isWork = false;
    }

    private void Write(string data)
    {
        try
        {
            TextWriter sw = new StreamWriter(path, true);
            sw.WriteLine(data);
            sw.Close();
        }
        catch(Exception e)
        {
            Console.WriteLine("Error" + e.Message);
        }
    }

    private void ReadDll()
    {
        oDynamic.Timing();
    }
}