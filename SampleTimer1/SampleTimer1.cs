﻿namespace SampleTimer1;
public class SampleTimer1
{
    public string Name { get; set; }
    public bool isWork = true;
    private string path = "log.txt";
    public SampleTimer1()
    {
        Name = "SampleTimer1";
    }

    public void Timing()
    {
        while(isWork)
        {
            DateTime dt = DateTime.Now;
            string data = $"{Name} - {dt.Hour}:{dt.Minute} - {dt.Second}";
            Write(data);
            Console.WriteLine($"{Name} - {dt.Hour}:{dt.Minute} - {dt.Second}");
            Thread.Sleep(1000);
        }
    }

    public void Write(string data)
    {
        try
        {
            TextWriter sw = new StreamWriter(path, true);
            sw.WriteLine(data);
            sw.Close();
        }
        catch(Exception e)
        {
            Console.WriteLine("Error" + e.Message);
        }
    }
}
